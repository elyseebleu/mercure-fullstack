export interface User extends Document {
  readonly name: String;
  readonly password: String;
}
